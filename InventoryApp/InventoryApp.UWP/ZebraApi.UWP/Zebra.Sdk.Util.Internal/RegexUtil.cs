﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Zebra.Sdk.Util.Internal
{
    internal class RegexUtil
    {
        public RegexUtil()
        {
        }

        public static List<string> GetMatches(string regex, string s)
        {
            List<string> retVal = new List<string>();
            Match m = (new Regex(regex)).Match(s);
            if (m.Success)
            {
                for (int i = 0; i < m.Groups.Count; i++)
                {
                    retVal.Add(m.Groups[i].Value);
                }
            }
            return retVal;
        }
    }
}
