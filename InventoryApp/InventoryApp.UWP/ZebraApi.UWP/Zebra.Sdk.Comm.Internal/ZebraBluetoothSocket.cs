﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Networking.Sockets;
using Zebra.Sdk.Comm;

namespace Zebra.Sdk.Comm.Internal
{
    public class ZebraBluetoothSocket : ZebraSocket
    {
        private StreamSocket socket;

        private RfcommDeviceService service;

        private SocketProtectionLevel protectionLevel;

        private BluetoothDevice bluetoothDevice;

        /// <summary>
        /// 35 seconds was chosen for a Max Timeout so that if the pairing request is 
        /// ignored it will close and deny the pair before we hit the Max Timeout and close.
        /// </summary>
        private const int MAX_TIMEOUT = 35000;
        public ZebraBluetoothSocket(RfcommDeviceService service, SocketProtectionLevel protectionLevel, BluetoothDevice bluetoothDevice)
        {
            this.service = service;
            this.protectionLevel = protectionLevel;
            this.bluetoothDevice = bluetoothDevice;
            this.socket = new StreamSocket();
        }

        public void Close()
        {
            this.socket.Dispose();
            this.service.Dispose();
            this.bluetoothDevice.Dispose();
        }

        public void Connect()
        {
            try
            {
                this.socket.ConnectAsync(this.service.ConnectionHostName, this.service.ConnectionServiceName, this.protectionLevel).AsTask().Wait();
            }
            catch (Exception exception)
            {
                Exception e = exception;
                this.Close();
                throw new ConnectionException(e.Message, e);
            }
        }

        public BinaryReader GetInputStream()
        {
            return new BinaryReader(WindowsRuntimeStreamExtensions.AsStreamForRead(this.socket.InputStream));
        }

        public BinaryWriter GetOutputStream()
        {
            return new BinaryWriter(this.socket.OutputStream.AsStreamForWrite());
        }
    }
}
