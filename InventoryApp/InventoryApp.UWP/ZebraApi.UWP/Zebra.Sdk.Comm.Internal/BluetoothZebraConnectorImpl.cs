﻿using System;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Comm.Internal;

namespace Zebra.Sdk.Comm.Internal
{
    internal class BluetoothZebraConnectorImpl : ZebraConnector
    {
        protected string macAddress;

        protected Guid channelId = BluetoothUuids.PRINTING_CHANNEL_ID;

        public BluetoothZebraConnectorImpl(string macAddress, ConnectionChannel channel)
        {
            this.macAddress = macAddress;
            if (channel == ConnectionChannel.STATUS_CHANNEL)
            {
                this.channelId = BluetoothUuids.STATUS_CHANNEL_ID;
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        /// <exception cref="T:Zebra.Sdk.Comm.ConnectionException"></exception>
        /// <exception cref="T:System.ArgumentException"></exception>
        public virtual ZebraSocket Open()
        {
            return BluetoothDeviceHelper.OpenConnection(this.macAddress, this.channelId, SocketProtectionLevel.BluetoothEncryptionWithAuthentication).Result;
        }
    }
}
