﻿using System;
using System.Threading.Tasks;

namespace Zebra.Sdk.Util.Internal
{
    /// <summary>
    /// Wrapper class for System.Threading.Tasks Task functions. For internal use of the Zebra Printer API only.
    /// </summary>
    internal class Sleeper
    {
        private static Sleeper sleeper;

        protected Sleeper()
        {
        }

        private static Sleeper GetInstance()
        {
            if (Sleeper.sleeper == null)
            {
                Sleeper.sleeper = new Sleeper();
            }
            return Sleeper.sleeper;
        }

        protected virtual void PerformSleep(long millis)
        {
            try
            {
                Task.Delay((int)millis).Wait();
            }
            catch (Exception exception)
            {
            }
        }

        /// <summary>
        /// For internal use of the Zebra Printer API only. Wrapper for <see cref="M:System.Threading.Tasks.Task.Delay(System.Int32)" /> to not throw an 
        /// exception. Causes the currently executing thread to sleep (temporarily cease execution) for the specified number 
        /// of milliseconds. The thread does not lose ownership of any monitors.
        /// </summary>
        /// <param name="millis"></param>
        public static void Sleep(long millis)
        {
            Sleeper.GetInstance().PerformSleep(millis);
        }
    }
}
