﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Zebra.Sdk.Comm.Internal
{
    public class BluetoothHelper
    {
        private const int WINDOWS_10_MAJOR_VERSION = 10;
        public BluetoothHelper()
        {
        }
        public ulong ConvertMacAddressToUlong(string macAddress)
        {
            return Convert.ToUInt64(macAddress.Replace(":", ""), 16);
        }

        public string FormatMacAddress(string initialMacAddress)
        {
            if (initialMacAddress == null)
            {
                return null;
            }
            initialMacAddress = initialMacAddress.Trim();
            StringBuilder retVal = new StringBuilder();
            if (initialMacAddress.Length != 12)
            {
                retVal.Append(initialMacAddress);
            }
            else
            {
                retVal.Append(initialMacAddress.Substring(0, 2));
                retVal.Append(":");
                retVal.Append(initialMacAddress.Substring(2, 2));
                retVal.Append(":");
                retVal.Append(initialMacAddress.Substring(4, 2));
                retVal.Append(":");
                retVal.Append(initialMacAddress.Substring(6, 2));
                retVal.Append(":");
                retVal.Append(initialMacAddress.Substring(8, 2));
                retVal.Append(":");
                retVal.Append(initialMacAddress.Substring(10, 2));
            }
            return retVal.ToString().ToUpper();
        }

        public bool IsBluetoothConnectionString(string descriptionString)
        {
            return Regex.IsMatch(descriptionString, "(BT_MULTI:|BT:|BT_STATUS:)", RegexOptions.IgnoreCase);
        }

        public bool IsWindows10()
        {
            bool isWindows10 = false;

            //Get Operating system information.
            OperatingSystem os = Environment.OSVersion;
            //Get version information about the os.
            Version vs = os.Version;
            if(os.Platform == PlatformID.Win32NT)
            {
                isWindows10 = vs.Major == 10;
            }

            //MatchCollection match = (new Regex("(\\d+)")).Matches(RuntimeInformation.OSDescription);
            //if (match.Count > 0)
            //{
            //    isWindows10 = Convert.ToInt32(match[0].Value) >= 10;
            //}
            return isWindows10;
        }
    }
}
