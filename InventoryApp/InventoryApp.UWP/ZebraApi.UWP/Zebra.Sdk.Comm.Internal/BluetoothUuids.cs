﻿using System;

namespace Zebra.Sdk.Comm.Internal
{
    internal class BluetoothUuids
    {
        internal readonly static Guid STATUS_CHANNEL_ID;

        internal readonly static Guid PRINTING_CHANNEL_ID;

        static BluetoothUuids()
        {
            BluetoothUuids.STATUS_CHANNEL_ID = new Guid("AEB33570-0B7B-11E3-8FFD-0800200C9A66");
            BluetoothUuids.PRINTING_CHANNEL_ID = new Guid("00001101-0000-1000-8000-00805F9B34FB");
        }

        public BluetoothUuids()
        {
        }
    }
}
