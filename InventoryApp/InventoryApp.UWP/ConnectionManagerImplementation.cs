﻿using InventoryApp.Interfaces;
using InventoryApp.UWP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Enumeration;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using Xamarin.Forms;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer.Discovery;

[assembly: Dependency(typeof(ConnectionManagerImplementation))]
namespace InventoryApp.UWP
{
    public class ConnectionManagerImplementation : IConnectionManager
    {
        public string BuildBluetoothConnectionChannelsString(string macAddress)
        {
            throw new NotImplementedException();
        }

        public void FindBluetoothPrinters(DiscoveryHandler discoveryHandler)
        {
            BluetoothDiscoverer.FindPrinters(discoveryHandler);
        }

        public Connection GetBluetoothConnection(string macAddress)
        {
            return new BluetoothConnection(macAddress);
        }

        public StatusConnection GetBluetoothStatusConnection(string macAddress)
        {
            //return new BluetoothStatusConnection(macAddress);
            throw new NotImplementedException();
        }

        public MultichannelConnection GetMultichannelBluetoothConnection(string macAddress)
        {
            //return new MultichannelBluetoothConnection(macAddress);
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<DiscoveredPrinter>> GetPairedBluetoothDevices()
        {
            var pairedDevicesSelectors = Windows.Devices.Bluetooth.BluetoothDevice.GetDeviceSelectorFromPairingState(true);
            var pairedDevices = (await DeviceInformation.FindAllAsync(pairedDevicesSelectors)).ToList();

            var discoveredDevices = new List<DiscoveredPrinterBluetooth>();
            foreach (var device in pairedDevices)
            {
                var bluetoothDevice = await BluetoothDevice.FromIdAsync(device.Id);
                discoveredDevices.Add(new DiscoveredPrinterBluetooth(
                    string.Join(":", BitConverter.GetBytes(bluetoothDevice.BluetoothAddress).Reverse()
                        .Select(b => b.ToString("X2"))).Substring(6),
                    device.Name));
            }
            
            //var discoveredDevices = pairedDevices.Select(x => new DiscoveredPrinterBluetooth(x.Name, x.Id)).ToArray();
            return discoveredDevices;
        }
    }
}
