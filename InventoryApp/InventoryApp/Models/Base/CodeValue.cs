﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class CodeValue
    {
        [JsonProperty("value_member")]
        public string Code { get; set; }
        [JsonProperty("display_member")]
        public string Value { get; set; }
        public CodeValue()
        {
        }
        public CodeValue(string code, string value)
        {
            Code = code;
            Value = value;
        }
    }

    public class CodeValueMap
    {
        [JsonProperty("parent_group_name")]
        public string ParentGroupName { get; set; }
        [JsonProperty("parent_value")]
        public string ParentCode { get; set; }
        [JsonProperty("value_member")]
        public string Code { get; set; }
        [JsonProperty("display_member")]
        public string Value { get; set; }
    }

    public static class CodeValueFactory
    {
        public static List<string> RegenerationLocationList = new List<string>
        {
            "Lima",
            "Huancayo",
            "San Ramón",
        };
        static List<CodeValue> _methodList = new List<CodeValue> {
            new CodeValue( "2", "Regeneration"),
            new CodeValue( "3", "Distribution"),
            new CodeValue( "4", "Correct Errors (daily work)"),
            new CodeValue( "5", "Inventory Adjustments (annual activity) "),
            new CodeValue( "6", "Other"),
            new CodeValue( "7", "Safety Duplicates International"),
            new CodeValue( "8", "DNA for Conservation"),
            new CodeValue( "9", "Svalbar")
        };
        static List<CodeValue> _actionNameCodeList = new List<CodeValue>{
            new CodeValue( "INCREASE", "Increase"),
            new CodeValue( "DISCOUNT", "Discount")
        };
        public static List<CodeValue> OrderIntendedUseList = new List<CodeValue>
        {
            // ORDER_INTENDED_USE
            //new CodeValue("1","Agriculture"),
            //new CodeValue("2","Breeding"),
            //new CodeValue("3","Commercial Sector"),
            //new CodeValue("4","Education"),
            //new CodeValue("5","Research"),
            // ORDER_INTENDED_USE_CIP
            new CodeValue("20","Identity verification"),
            new CodeValue("21","Regeneration/Viability testing"),
            new CodeValue("22","Characterization"),
            new CodeValue("23","Cryopreservation"),
            new CodeValue("24","Phytosanitary"),
            new CodeValue("26","Safety Backup"),
            new CodeValue("27","Repatriation"),
            new CodeValue("28","Research in Genebank"),
            new CodeValue("77","Rejuvenation"),
        };
        public static List<CodeValue> OrderRequestTypeList = new List<CodeValue>
        {
            new CodeValue("BA","Backup"),
            new CodeValue("DI","Distribution"),
            new CodeValue("GI","Germplasm introduction"),
            new CodeValue("GR","Germination"),
            new CodeValue("HE","Herbarium/reidentification"),
            new CodeValue("IO","Information only"),
            new CodeValue("NR","Non-research, non-educational"),
            new CodeValue("OB","Observation/evaluation"),
            new CodeValue("PT","Phytosanitary Testing"),
            new CodeValue("RE","Replenishment/regrow"),
            new CodeValue("RP","Repatriation"),
            new CodeValue("TR","Transfer"),
        };
        public static List<CodeValue> RegenerationOrderRequestTypeList = new List<CodeValue>
        {
            //new CodeValue("GR","Germination"),
            //new CodeValue("RE","Replenishment/regrow"),
            new CodeValue("GR","Prueba de viabilidad"),
            new CodeValue("RE","Regeneración"),
        };
        public static List<CodeValue> FormTypeList = new List<CodeValue>
        {
            //new CodeValue("**","**"),
            //new CodeValue("BE","BE"),
            //new CodeValue("CR","CR"),
            //new CodeValue("CT","CT"),
            //new CodeValue("DN","DN"),
            //new CodeValue("FD","FD"),
            //new CodeValue("FO","FO"),
            //new CodeValue("HE","HE"),
            //new CodeValue("HS","HS"),
            //new CodeValue("IO","IO"),
            //new CodeValue("IV","IV"),
            //new CodeValue("LP","LP"),
            //new CodeValue("LV","LV"),
            //new CodeValue("PI","PI"),
            //new CodeValue("PL","PL"),
            //new CodeValue("PO","PO"),
            //new CodeValue("RE","RE-SD"),
            //new CodeValue("RN","RN"),
            //new CodeValue("RT","RT"),
            //new CodeValue("SA","SA"),
            //new CodeValue("SD","SD"),
            //new CodeValue("SH","SH"),
            //new CodeValue("SP","SP"),
            //new CodeValue("TU","TU"),
            new CodeValue("DN","ADN"),
            new CodeValue("BE","Baya"),
            new CodeValue("SP","Brote"),
            new CodeValue("CR","Criopreservado"),
            new CodeValue("CT","Esqueje"),
            new CodeValue("SH","Folio"),
            new CodeValue("FO","Follaje"),
            new CodeValue("FD","Liofilizado"),
            new CodeValue("HE","Muestra de herbario"),
            new CodeValue("HS","Semilla híbrida"),
            new CodeValue("IV","In vitro"),
            new CodeValue("IO","Sólo la información"),
            new CodeValue("LV","Hojas"),
            new CodeValue("PI","Pistilos"),
            new CodeValue("PL","Planta"),
            new CodeValue("PO","Polen"),
            new CodeValue("RE","Semilla para regeneración"),
            new CodeValue("RN","ARN"),
            new CodeValue("RT","Rizoma"),
            new CodeValue("SD","Semilla botánica"),
            new CodeValue("SA","Starch"),
            new CodeValue("LP","Tejido liofilizada"),
            new CodeValue("TU","Tuberculo"),
        };
        public static List<CodeValue> InventoryContainerList = new List<CodeValue>
        {
            new CodeValue("OTHER","Otro"),
            new CodeValue("JIFFY6","Jiffy Nro 6"),
            new CodeValue("JIFFY7","Jiffy Nro 7"),
            new CodeValue("POT","Maceta"),
        };
        public static List<CodeValue> CrossingTypeList = new List<CodeValue>
        {
            new CodeValue("SC","Sib-cross"),
            new CodeValue("AP","Autofecundación"),
            new CodeValue("BK","Bulk"),
            new CodeValue("LP","Libre polinización"),
        };


        public static List<CodeValue> OrderRequestActionList = new List<CodeValue>
        {
            new CodeValue("CANCEL","Cancel or abort order"),
            new CodeValue("CURALERTED","Curator alerted about order"),
            new CodeValue("CURATOR","Curator assigned"),
            new CodeValue("CURCLEARED","Curator cleared an order"),
            new CodeValue("CURWAIPROP","Order waiting for IP expiration"),
            new CodeValue("CURWAREGEN","Order waiting for regeneration"),
            new CodeValue("DONE","Completed order"),
            new CodeValue("ESSREQUEST","Endangered Species Statement requirement"),
            new CodeValue("FORWARD","Forward order to another site"),
            new CodeValue("GPAALERTED","Curator alerted Specialist on order"),
            new CodeValue("HOLD","Hold order pending action"),
            new CodeValue("IMPORT_PER","Import Permit requested"),
            new CodeValue("IMPPMTREQ","Requestor notified of need for permit"),
            new CodeValue("INSPECT","Order sent to APHIS"),
            new CodeValue("INSPECTASKED","Export requirements requested"),
            new CodeValue("INSPECTSITE","Site phyto inspection needed"),
            new CodeValue("NEW","New Order"),
            new CodeValue("ORDFILLED","Order filled ready to ship"),
            new CodeValue("PENDING","Order pending"),
            new CodeValue("PSHIP","Partial shipment"),
            new CodeValue("QUALITYPASSED","Quality test passed"),
            new CodeValue("QUALITYTEST","Quality test needed and sent"),
            new CodeValue("RECEIVED","Order was received by recipient"),
            new CodeValue("REQASKED","Requestor solicited for addional info"),
            new CodeValue("RETURNED","Returned order"),
            new CodeValue("SHIPPED","Order shipped"),
            new CodeValue("SMTAACCEPT","Documentation available, SMTA accepted"),
            new CodeValue("SMTASTATUS","SMTA status"),
            new CodeValue("SPLIT","Order split into sub-orders"),
        };
        public static List<CodeValue> OrderRequestItemActionList = new List<CodeValue>
        {
            //new CodeValue("OUT_STORAGE","Retrieve from storage"),
            //new CodeValue("PACK","Pack"),
            //new CodeValue("VERIFY_ITEM","Verifiy item"),
            new CodeValue("OUT_STORAGE","Retirar del almacenamiento"),
            new CodeValue("PACK","Empaquetar"),
            new CodeValue("VERIFY_ITEM","Verificar item de solicitud"),
        };
        public static List<CodeValue> QuantityUnitList = new List<CodeValue>
        {
            //new CodeValue("ct","count"),
            //new CodeValue("cu","cuttings"),
            //new CodeValue("gm","gram"),
            //new CodeValue("kc","count x 1000"),
            //new CodeValue("kg","kilograms"),
            //new CodeValue("mg","milligrams"),
            //new CodeValue("ml","milliliters"),
            //new CodeValue("ng","nanograms"),
            //new CodeValue("pk","packet"),
            //new CodeValue("tb","tubes"),
            //new CodeValue("ug","micrograms"),
            //new CodeValue("vl","Vial"),
            new CodeValue("ct","Conteo"),
            new CodeValue("kc","Conteo x 1000"),
            new CodeValue("cu","Esquejes"),
            new CodeValue("gm","Gramos"),
            new CodeValue("kg","Kilogramos"),
            new CodeValue("ug","Microgramos"),
            new CodeValue("mg","Miligramos"),
            new CodeValue("ml","Mililitros"),
            new CodeValue("ng","Nanogramos"),
            new CodeValue("pk","Paquete"),
            new CodeValue("tb","Tubos"),
            new CodeValue("vl","Vial"),
        };

        public static List<CodeValue> MethodList { get { return _methodList; } }
        public static List<CodeValue> ActionNameCodeList { get { return _actionNameCodeList; } }

        public static List<string> GetUnits()
        {
            return new List<string> { "each", "grams", "milligrams", "Botanic seed", "Grams of seed", "Berry", "Plant" };
        }
        public static List<string> GetSearchInventoryFilters()
        {
            return new List<string> { "Accession Number", "Accession Name", "Inventory Id / Lot Id"
                , "Collecting number","Storage location level 2", "Storage location level 3", "Note", "Order request - Local number", "Order request - Order Id"
                /* "Distribution Request",*/ /*"List Name",*/ 
                /*"Accession ID", "Inventory Number",*/ };
        }
        public static List<string> GetSearchInventoryActivities()
        {
            return new List<string> { "Wild Potato", "Sweetpotato", "ARTC" };
        }
        public static List<string> GetSearchInventoryLocations()
        {
            return new List<string> { "Chamber A (0 C)", "Chamber B (-10 C)", "Chamber C (-20 C)" };
        }

        public static List<string> GetContainerTypeList()
        {
            return new List<string> { "Paper Envelope 8.6x5.8 cm", "Paper Envelope 18.2x10.2 cm", "Aluminium Envelope 8x12 cm", "Aluminium Envelope 21.5x17 cm", "Cardboard Folkote 29x42 cm", "Eppendorf" };
        }

        #region Printing
        public static List<string> GetPrinterList()
        {
            return new List<string> { "\\\\CIP0977\\Printer", "10.10.10.20", "10.10.10.30" };
        }
        public static List<string> GetLabelDesignList()
        {
            return new List<string> { "Seed Label", "ADN Label", "Herbarium Label" };
        }
        #endregion

        #region WelcomePage

        public static List<string> GetWorkgroupList()
        {
            return new List<string> { "Wild Potato", "Cultivated Potato", "Sweetpotato", "ARTC" };
        }
        public static List<string> GetLocationList()
        {
            return new List<string> { "Chamber A", "Chamber B", "Chamber C" };
        }

        #endregion
    }
}
