﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class GroupSetting
    {
        public int id { get; set; }
        public int sys_group_id { get; set; }
        public string dataview_name { get; set; }
        public string dataview_field_name { get; set; }
        public string gui_hint { get; set; }
        public string gui_filter { get; set; }
    }
}
