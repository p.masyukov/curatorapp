﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class MenuItem
    {
        public string Text { get; set; }
        public string Path { get; set; }
        public string BackgroundColor { get; set; } = "White";
    }
}
