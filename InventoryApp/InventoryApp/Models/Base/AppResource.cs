﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class AppLangResource
    {
        //public int app_resource_id { get; set; }
        public int sys_lang_id { get; set; }
        //public string app_name { get; set; } //GRINGlobalMobileApp
        public string form_name { get; set; }
        public string app_resource_name { get; set; }
        //public string description { get; set; }
        public string display_member { get; set; }
        //public string value_member { get; set; }

    }
}
