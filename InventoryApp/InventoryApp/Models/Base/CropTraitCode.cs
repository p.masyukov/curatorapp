﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace InventoryApp.Models
{
    public class CropTraitCode
    {
        [JsonProperty("crop_trait_code_id")]
        public int CropTraitCodeId { get; set; }

        [JsonProperty("code")]
        public string CodeValue { get; set; }

        [JsonProperty("title")]
        public string CodeTitle { get; set; }

        [JsonProperty("crop_trait_id")]
        public int CropTraitId { get; set; }
    }
}
