﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class CropTraitObservation : BindableBase
    {
        public int crop_trait_observation_id { get; set; }
        public int inventory_id { get; set; }
        public int crop_trait_id { get; set; }
        public int? crop_trait_code_id { get; set; }
        public string code { get; set; }
        public decimal? numeric_value { get; set; }
        public string string_value { get; set; }
        //public string crop_trait_name { get; set; }
        private string _DisplayText;
        public string DisplayText
        {
            get { return _DisplayText; }
            set { SetProperty(ref _DisplayText, value); }
        }
        public Guid? Guid { get; set; }
        public DateTime created_date { get; set; }
        public int created_by { get; set; }
        public DateTime? modified_date { get; set; }
        public int? modified_by { get; set; }
    }

    public class CropTrait
    {
        [JsonProperty("crop_trait_id")]
        public int CropTraitId { get; set; }
        [JsonProperty("coded_name")]
        public string CropTraitName { get; set; }
        [JsonProperty("data_type_code")]
        public string DataTypeCode { get; set; }
        [JsonProperty("is_coded")]
        public char IsCoded { get; set; }
        [JsonProperty("max_length")]
        public int? max_length { get; set; }
        [JsonProperty("numeric_format")]
        public string NumericFormat { get; set; }
        [JsonProperty("numeric_maximum")]
        public decimal? NumericMaximum { get; set; }
        [JsonProperty("numeric_minimum")]
        public decimal? NumericMinimum { get; set; }
    }
}
