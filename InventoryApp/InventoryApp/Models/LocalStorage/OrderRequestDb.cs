﻿using SQLite;
using System;

namespace InventoryApp.Models.LocalStorage
{
    [Table("order_request")]
    public class OrderRequestDb
    {
        [PrimaryKey]
        public int OrderRequestId { get; set; }
        public string LocalNumber { get; set; }
        public string OrderTypeCode { get; set; }
        public DateTime? LastSyncedDate{ get; set; }
    }
}
