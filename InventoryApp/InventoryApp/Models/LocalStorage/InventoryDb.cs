﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    [Table("inventory")]
    public class InventoryDb
    {
        public int InventoryId { get; set; }
        public string InventoryNumberPart1 { get; set; }
        public int? InventoryNumberPart2 { get; set; }
        public string InventoryNumberPart3 { get; set; }

        public string FormTypeCode { get; set; }
        public int InventoryMaintPolicyId { get; set; }
        public string IsDistributable { get; set; }

        public string StorageLocationPart1 { get; set; }
        public string StorageLocationPart2 { get; set; }
        public string StorageLocationPart3 { get; set; }
        public string StorageLocationPart4 { get; set; }
        public string IsAvailable { get; set; }
        public string AvailabilityStatusCode { get; set; }
        public decimal? QuantityOnHand { get; set; }
        public string QuantityOnHandUnitCode { get; set; }
        public string IsAutoDeducted { get; set; }
        public int AccessionId { get; set; }
        public int? ParentInventoryId { get; set; }
        public DateTime? PropagationDate { get; set; }
        public string PropagationDateCode { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime OwnedDate { get; set; }
        public int OwnedBy { get; set; }

        public int? OrderRequestId { get; set; }
        [PrimaryKey]
        public string Guid { get; set; }
        public string AccessionNumber { get; set; }
        public string InventoryNumber { get; set; }
        public string ContainerCode { get; set; }
        public bool IsMother { get; set; }
    }
}
