﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    public class LabelTemplateDb
    {
        public string TemplateCode { get; set; }
        public string TemplateName { get; set; }
        public string Zpl { get; set; }
        public IEnumerable<string> ParameterKeys { get; set; }
    }

    public static class LabelTemplateDbFactory
    {
        public static IEnumerable<LabelTemplateDb> LabelTemplateList => new List<LabelTemplateDb>()
        {
            new LabelTemplateDb
            {
                TemplateCode = "PLATE",
                TemplateName = "Placa",
                Zpl = @"
^XA
^MMT
^PW440
^LL0208
^LS0
^FT25,40^A0N,37,36^FH\^FD{1}^FS
^FT25,60^AAN,18,10^FH\^FD{2}^FS
^FT25,90^A0N,23,24^FH\^FD{3}^FS
^BY60,60^FT25,155^BXN,6,200,0,0,1,~
^FH\^FD{4}^FS
^FT25,175^A0N,16,16^FH\^FD{4}^FS
^FT100,115^A0N,16,16^FH\^FD{5}^FS
^FT100,135^A0N,16,16^FH\^FD{6}^FS
^FT100,155^A0N,16,16^FH\^FD{7}^FS
^PQ{0},0,1,Y^XZ",
                ParameterKeys = Enumerable.Empty<string>()
            },
            new LabelTemplateDb
            {
                TemplateCode = "GENERAL",
                TemplateName = "General",
                Zpl = @"
^XA
^MMT
^PW440
^LL0207
^LS0
^FT25,40^A0N,28,28^FH\^FD{1}^FS
^FT25,80^A0N,28,28^FH\^FD{2}^FS
^FT255,180^BQN,2,7
^FH\^FDLA,{1}^FS
^FT25,120^A0N,28,28^FH\^FD{3}^FS
^FT25,160^A0N,28,28^FH\^FD{4}^FS
^FT200,160^A0N,28,28^FH\^FD{5}^FS
^PQ{0},0,1,Y^XZ",
                ParameterKeys = new List<string>{ "AccessionNumber", "SpeciesCode", "Locality", "Date", "BrotherNumber" } },
            new LabelTemplateDb
            {
                TemplateCode = "POT",
                TemplateName = "Maceta",
                Zpl = @"
^XA
^MMT
^PW440
^LL0207
^LS0
^FT25,40^A0N,28,28^FH\^FD{1}^FS
^FT25,80^A0N,28,28^FH\^FD{2}^FS
^FT255,180^BQN,2,7
^FH\^FDLA,{6}^FS
^FT25,120^A0N,28,28^FH\^FD{3}^FS
^FT25,160^A0N,28,28^FH\^FD{4}^FS
^FT200,160^A0N,28,28^FH\^FD{5}^FS
^PQ{0},0,1,Y^XZ",
                ParameterKeys = new List<string>{ "AccessionNumber", "SpeciesCode", "Locality", "Date", "BrotherNumber", "Barcode" } },
        };
    }
}
