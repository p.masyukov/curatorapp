﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    [Table("inventory_harvest")]
    public class InventoryHarvestDb
    {
        public int? HarvestPollinatedFlowers { get; set; }
        public int? HarvestedFruits { get; set; }
        public DateTime? HarvestPrintedDate { get; set; }
        public DateTime? HarvestDate { get; set; }
        public int HarvestCooperatorId { get; set; }
        public string HarvestCooperator { get; set; }
        public string HarvestSummary { get; set; }
        public string HarvestLocality { get; set; }
        public int OrderRequestId { get; set; }
        [PrimaryKey]
        public string Guid { get; set; }
        public int MotherInventoryGuid { get; set; } //FemaleInventory
        public int? MotherInventoryChildOrder { get; set; } //FemaleOrder
        public string CrossingGuid { get; set; }
        public string HarvestTypeCode { get; set; } //FRUIT, TUBER

        public int? TubersCount { get; set; }
        public string TuberOrigin { get; set; }
        public decimal HarvestWeight { get; set; }
        //public DateTime? TuberHarvestDate { get; set; }
        //public string TuberHaverstLocality { get; set; }
        //public int? TuberHarvestCooperatorId { get; set; }
        //public string TuberHarvestCooperator { get; set; }

    }
}
