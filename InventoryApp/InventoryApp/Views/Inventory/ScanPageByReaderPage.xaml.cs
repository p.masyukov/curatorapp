﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InventoryApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScanPageByReaderPage : ContentPage
    {
        public ScanPageByReaderPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<ViewModels.ScanPageViewModel>(this, "SearchEntry.Focus",
            (vm) =>
            {
                try
                {
                    mSearchBar.Focus();
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.Fail(ex.Message + System.Environment.NewLine + ex.InnerException?.Message);
                }
            });
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<ViewModels.ScanPageViewModel>(this, "SearchEntry.Focus");

            base.OnDisappearing();
        }
    }
}