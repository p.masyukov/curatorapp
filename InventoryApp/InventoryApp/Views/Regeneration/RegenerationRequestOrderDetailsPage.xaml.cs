﻿using InventoryApp.Helpers;
using Xamarin.Forms;

namespace InventoryApp.Views
{
    public partial class RegenerationRequestOrderDetailsPage : ContentPage
    {
        public RegenerationRequestOrderDetailsPage()
        {
            InitializeComponent();
        }

        private void ExportToExcel(object sender, System.EventArgs e)
        {
            try
            {
                UserInterfaceUtils.ExportToExcel(dataGrid);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }
    }
}
