﻿using InventoryApp.Enums;
using InventoryApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace InventoryApp.Custom
{
    public class CropTraitObservationDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate CharTemplate { get; set; }
        public DataTemplate TextTemplate { get; set; }
        public DataTemplate NumericTemplate { get; set; }
        public DataTemplate IsCodedTemplate { get; set; }
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            EditingCropTraitObservation cto = (EditingCropTraitObservation)item;
            if (cto.IsCoded)
            {
                return IsCodedTemplate;
            }
            else if (cto.DataType.Equals(CropTraitDataTypeEnum.Numeric.GetString()))
            {
                return NumericTemplate;
            }
            else if (cto.DataType.Equals(CropTraitDataTypeEnum.Char.GetString()))
            {
                return CharTemplate;
            }
            else
            {
                return TextTemplate;
            }
        }
    }
}
