﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class HarvestTuberPageViewModel : ViewModelBaseZ
    {
        IOrderRequestRepository _orderRequestRepository;
        IInventoryLocalRepository _inventoryLocalRepository;

        private OrderRequestDb _SelectedOrderRequestDb;
        public OrderRequestDb SelectedOrderRequestDb
        {
            get { return _SelectedOrderRequestDb; }
            set { SetProperty(ref _SelectedOrderRequestDb, value); }
        }
        private IEnumerable<InventoryDb> _InventoryDbList;
        public IEnumerable<InventoryDb> InventoryDbList
        {
            get { return _InventoryDbList; }
            set { SetProperty(ref _InventoryDbList, value); }
        }
        private InventoryDb _selectedInventoryDb;
        public InventoryDb SelectedInventoryDb
        {
            get { return _selectedInventoryDb; }
            set { SetProperty(ref _selectedInventoryDb, value); }
        }
        private int _inventoryDbListCount;
        public int InventoryDbListCount
        {
            get { return _inventoryDbListCount; }
            set { SetProperty(ref _inventoryDbListCount, value); }
        }
        private IEnumerable<string> _TuberOriginList;
        public IEnumerable<string> TuberOriginList
        {
            get { return _TuberOriginList; }
            set { SetProperty(ref _TuberOriginList, value); }
        }
        private string _TuberOrigin;
        public string TuberOrigin
        {
            get { return _TuberOrigin; }
            set { SetProperty(ref _TuberOrigin, value); }
        }
        private decimal _TuberWeight;
        public decimal TuberWeight
        {
            get { return _TuberWeight; }
            set { SetProperty(ref _TuberWeight, value); }
        }
        private IEnumerable<int> _TuberCountList;
        public IEnumerable<int> TuberCountList
        {
            get { return _TuberCountList; }
            set { SetProperty(ref _TuberCountList, value); }
        }
        private int _TuberCount;
        public int TuberCount
        {
            get { return _TuberCount; }
            set { SetProperty(ref _TuberCount, value); }
        }
        private string _AccNumber;
        public string AccNumber
        {
            get { return _AccNumber; }
            set { SetProperty(ref _AccNumber, value); }
        }
        private DateTime _TuberHarvestDate;
        public DateTime TuberHarvestDate
        {
            get { return _TuberHarvestDate; }
            set { SetProperty(ref _TuberHarvestDate, value); }
        }

        public HarvestTuberPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;

            TuberOriginList = new List<string>() { "S", "T", "I", "E"};
            TuberCountList = new List<int>() { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
            TuberCount = -1;

            SelectInventoryDbCommand = new DelegateCommand(ExecuteSelectInventoryDbCommand).ObservesCanExecute(() => IsNotBusy);
            SaveAndPrintCommand = new DelegateCommand(ExecuteSaveAndPrintCommand).ObservesCanExecute(() => IsNotBusy);

            TuberHarvestDate = DateTime.Now;
        }
        public DelegateCommand SelectInventoryDbCommand { get; }
        private async void ExecuteSelectInventoryDbCommand()
        {
            try
            {
                IsBusy = true;

                AccNumber = SelectedInventoryDb.AccessionNumber;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SaveAndPrintCommand { get; }
        private async void ExecuteSaveAndPrintCommand()
        {
            try
            {
                IsBusy = true;

                SelectedInventoryDb = null;
                TuberOrigin = null;
                TuberWeight = 0;
                TuberCount = -1;
                AccNumber = string.Empty;

                await PageDialogService.DisplayAlertAsync("Resultado de guardar e imprimir", "Se registró correctamente", "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                var orderRequestList = await _orderRequestRepository.GetOrderRequestsAsync();

                if (Settings.SelectedRegenerationOrderRequestId == -1)
                    throw new Exception("Seleccionar solicitud en Ajustes de regeneración");

                SelectedOrderRequestDb = orderRequestList.FirstOrDefault(x => x.OrderRequestId == Settings.SelectedRegenerationOrderRequestId);
                
                var allOrderRequestInventories = await _inventoryLocalRepository.GetInventoriesByOrderRequestIdAsync(SelectedOrderRequestDb.OrderRequestId);
                InventoryDbList = allOrderRequestInventories.Where(x => x.IsMother
                    && !string.IsNullOrEmpty(x.InventoryNumberPart3) && !x.InventoryNumberPart3.Contains('.'));
                InventoryDbListCount = InventoryDbList.Count();

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
