﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class TransplantInventoriesPageViewModel : ViewModelBaseZ
    {
        private readonly IInventoryLocalRepository _inventoryLocalRepository;

        public IEnumerable<Inventory> SelectedInventories { get; set; }

        private IEnumerable<CodeValue> _formTypeCodeListt;
        public IEnumerable<CodeValue> FormTypeCodeList
        {
            get { return _formTypeCodeListt; }
            set { SetProperty(ref _formTypeCodeListt, value); }
        }
        private IEnumerable<CodeValue> _containerCodeList;
        public IEnumerable<CodeValue> ContainerCodeList
        {
            get { return _containerCodeList; }
            set { SetProperty(ref _containerCodeList, value); }
        }
        private CodeValue _selectedFormTypeCode;
        public CodeValue SelectedFormTypeCode
        {
            get { return _selectedFormTypeCode; }
            set { SetProperty(ref _selectedFormTypeCode, value); }
        }
        private CodeValue _selectedContainerCode;
        public CodeValue SelectedContainerCode
        {
            get { return _selectedContainerCode; }
            set { SetProperty(ref _selectedContainerCode, value); }
        }
        private IEnumerable<CodeValue> _quantityUnitCodeList;
        public IEnumerable<CodeValue> QuantityUnitCodeList
        {
            get { return _quantityUnitCodeList; }
            set { SetProperty(ref _quantityUnitCodeList, value); }
        }
        private CodeValue _selectedQuantityUnitCode;
        public CodeValue SelectedQuantityUnitCode
        {
            get { return _selectedQuantityUnitCode; }
            set { SetProperty(ref _selectedQuantityUnitCode, value); }
        }
        private string _storageLocation1;
        public string StorageLocation1
        {
            get { return _storageLocation1; }
            set { SetProperty(ref _storageLocation1, value); }
        }
        private string _storageLocation2;
        public string StorageLocation2
        {
            get { return _storageLocation2; }
            set { SetProperty(ref _storageLocation2, value); }
        }
        private string _storageLocation3;
        public string StorageLocation3
        {
            get { return _storageLocation3; }
            set { SetProperty(ref _storageLocation3, value); }
        }
        private string _storageLocation4;
        public string StorageLocation4
        {
            get { return _storageLocation4; }
            set { SetProperty(ref _storageLocation4, value); }
        }
        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }
        private decimal _quantity;
        public decimal Quantity
        {
            get { return _quantity; }
            set { SetProperty(ref _quantity, value); }
        }
        private bool _isMultiplyChecked;
        public bool IsMultiplyChecked
        {
            get { return _isMultiplyChecked; }
            set { SetProperty(ref _isMultiplyChecked, value); }
        }
        private int _inventoryChildrenCount;
        public int InventoryChildrenCount
        {
            get { return _inventoryChildrenCount; }
            set
            {
                SetProperty(ref _inventoryChildrenCount, value);
                Quantity = _inventoryChildrenCount * _inventoryChildrenQuantity;
            }
        }
        private int _inventoryChildrenQuantity;
        public int InventoryChildrenQuantity
        {
            get { return _inventoryChildrenQuantity; }
            set
            {
                SetProperty(ref _inventoryChildrenQuantity, value);
                Quantity = _inventoryChildrenCount * _inventoryChildrenQuantity;
            }
        }
        private bool _isMother;
        public bool IsMother
        {
            get { return _isMother; }
            set { SetProperty(ref _isMother, value); }
        }
        public TransplantInventoriesPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IInventoryLocalRepository inventoryLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _inventoryLocalRepository = inventoryLocalRepository;

            CancelCommand = new DelegateCommand(ExecuteCancelCommand).ObservesCanExecute(() => IsNotBusy);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
        }

        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            await NavigationService.GoBackAsync();
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                var minorInventoryId = await _inventoryLocalRepository.GetMinorInventoryIdAsync();
                minorInventoryId = Math.Min(minorInventoryId, 0) - 1;
                foreach (var inv in SelectedInventories)
                {
                    var currentVersion = inv.inventory_number_part3.Split('.');
                    var currentLastStep = int.Parse(currentVersion.Last());
                    currentVersion[currentVersion.Length - 1] = (currentLastStep + 1).ToString();
                    var nextVersion = string.Join(".", currentVersion);

                    var newInventoriesMaxIndex = IsMultiplyChecked ? InventoryChildrenCount : 0;
                    for (int i = 0; i <= newInventoriesMaxIndex; i++)
                    {
                        var newInventorySuffix = nextVersion + (i > 0 ? "." + i : string.Empty);
                        var dbInventory = new InventoryDb()
                        {
                            AccessionId = inv.accession_id,
                            AccessionNumber = inv.accession_number,
                            AvailabilityStatusCode = inv.availability_status_code,
                            ContainerCode = SelectedContainerCode.Code,
                            CreatedBy = inv.created_by,
                            CreatedDate = DateTime.UtcNow,
                            FormTypeCode = SelectedFormTypeCode.Code,
                            Guid = Guid.NewGuid().ToString(),
                            InventoryId = minorInventoryId--,
                            InventoryMaintPolicyId = inv.inventory_maint_policy_id,
                            InventoryNumber = $"{inv.inventory_number_part1} {inv.inventory_number_part2} {newInventorySuffix} {SelectedFormTypeCode.Code}",
                            InventoryNumberPart1 = inv.inventory_number_part1,
                            InventoryNumberPart2 = inv.inventory_number_part2,
                            InventoryNumberPart3 = newInventorySuffix,
                            IsAutoDeducted = inv.is_auto_deducted,
                            IsAvailable = inv.is_available,
                            IsDistributable = inv.is_distributable,
                            ModifiedBy = Settings.UserCooperatorId,
                            ModifiedDate = DateTime.UtcNow,
                            Note = Notes,
                            OrderRequestId = inv.order_request_id,
                            OwnedBy = inv.owned,
                            OwnedDate = DateTime.UtcNow,
                            ParentInventoryId = inv.inventory_id,
                            PropagationDate = DateTime.UtcNow,
                            PropagationDateCode = "MM/dd/yyyy",
                            QuantityOnHand = i == 0 ? Quantity : InventoryChildrenQuantity,
                            QuantityOnHandUnitCode = SelectedQuantityUnitCode.Code,
                            StorageLocationPart1 = StorageLocation1,
                            StorageLocationPart2 = StorageLocation2,
                            StorageLocationPart3 = StorageLocation3,
                            StorageLocationPart4 = StorageLocation4,
                            IsMother = IsMother,
                        };

                        await _inventoryLocalRepository.InsertInventoryAsync(dbInventory);
                    }

                    //Update inventory quantity
                    await _inventoryLocalRepository.PatchQuantityAsync(inv.Guid, 0, Settings.UserCooperatorId, DateTime.UtcNow);
                }

                await PageDialogService.DisplayAlertAsync("Resultado de transplantar inventarios", "Se crearon los inventarios correctamente", "OK");
                await NavigationService.GoBackAsync();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedInventories"))
            {
                SelectedInventories = parameters.GetValue<IEnumerable<Inventory>>("SelectedInventories");
            }
            else
                throw new Exception("SelectedInventories is required");

            var currentInventory = SelectedInventories.First();

            FormTypeCodeList = CodeValueFactory.FormTypeList;
            ContainerCodeList = CodeValueFactory.InventoryContainerList;
            SelectedFormTypeCode = FormTypeCodeList.FirstOrDefault();
            SelectedContainerCode = ContainerCodeList.FirstOrDefault();

            QuantityUnitCodeList = CodeValueFactory.QuantityUnitList;
            SelectedQuantityUnitCode = QuantityUnitCodeList.FirstOrDefault(x => x.Code.Equals(currentInventory.quantity_on_hand_unit_code));

            IsMultiplyChecked = false;
            InventoryChildrenCount = (int)currentInventory.quantity_on_hand.GetValueOrDefault(0);
            InventoryChildrenQuantity = 1;

            Quantity = currentInventory.quantity_on_hand.GetValueOrDefault(0);
        }
    }
}
