﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.ViewModels
{
    public class CrossToHarvestPageViewModel : ViewModelBaseZ
    {
        private readonly IRestService _restService;
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly ICropTraitObservationLocalRepository _cropTraitObservationLocalRepository;
        private readonly ICrossingLocalRepository _crossingLocalRepository;

        private IEnumerable<OrderRequest> _orderRequestList;
        public IEnumerable<OrderRequest> OrderRequestList
        {
            get { return _orderRequestList; }
            set { SetProperty(ref _orderRequestList, value); }
        }
        private OrderRequest _selectedOrderRequest;
        public OrderRequest SelectedOrderRequest
        {
            get { return _selectedOrderRequest; }
            set { SetProperty(ref _selectedOrderRequest, value); }
        }
        public IEnumerable<string> LocationList => CodeValueFactory.RegenerationLocationList;
        private string _selectedLocation;
        public string SelectedLocation
        {
            get { return _selectedLocation; }
            set
            {
                SetProperty(ref _selectedLocation, value);
                if(value != null && !value.Equals(Settings.SelectedRegenerationLocation))
                    Settings.SelectedRegenerationLocation = value;
            }
        }
        public bool IsHarvestGeneral
        { 
            get { return Settings.IsHarvestGeneral; }
            set { Settings.IsHarvestGeneral = value; }
        }
        public string BluetoothPrinterMac
        {
            get { return Settings.BluetoothPrinterMac; }
            set
            {
                Settings.BluetoothPrinterMac = value;
                RaisePropertyChanged(nameof(BluetoothPrinterMac));
            }
        }

        public CrossToHarvestPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService, IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository,
            ICropTraitObservationLocalRepository cropTraitObservationLocalRepository, ICrossingLocalRepository crossingLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _cropTraitObservationLocalRepository = cropTraitObservationLocalRepository;
            _crossingLocalRepository = crossingLocalRepository;

            SelectedOrderRequestChangedCommand = new DelegateCommand(ExecuteSelectedOrderRequestChangedCommand).ObservesCanExecute(() => IsNotBusy);
            DeleteOrderRequestCommand = new DelegateCommand(ExecuteDeleteOrderRequestCommand).ObservesCanExecute(() => IsNotBusy);
            SyncRegenerationOrderRequestCommand = new DelegateCommand(ExecuteSyncRegenerationOrderRequestCommand).ObservesCanExecute(() => IsNotBusy);
            ExploreOrderRequestCommand = new DelegateCommand(ExecuteExploreOrderRequestCommand).ObservesCanExecute(() => IsNotBusy);

            SetLocalPrinterCommand = new DelegateCommand(ExecuteSetLocalPrinterCommand).ObservesCanExecute(() => IsNotBusy);
        }
        public DelegateCommand SelectedOrderRequestChangedCommand { get; }
        private async void ExecuteSelectedOrderRequestChangedCommand()
        {
            if (!IsInitialized)
                return;

            try
            {
                IsBusy = true;

                if (SelectedOrderRequest == null)
                    return;

                Settings.SelectedRegenerationOrderRequestId = SelectedOrderRequest.order_request_id;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand DeleteOrderRequestCommand { get;}
        private async void ExecuteDeleteOrderRequestCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedOrderRequest == null)
                    return;

                _ = await _crossingLocalRepository.DeleteAllAsync();
                _ = await _cropTraitObservationLocalRepository.DeteleAllCropTraitObservationAsync();
                _ = await _inventoryLocalRepository.DeleteInventoryByOrderRequestIdAsync(SelectedOrderRequest.order_request_id);
                _ = await _orderRequestRepository.DeleteOrderRequestByIdAsync(SelectedOrderRequest.order_request_id);

                await LoadOrderRequests();
                Settings.SelectedRegenerationOrderRequestId = -1;
                _selectedOrderRequest = null;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SyncRegenerationOrderRequestCommand { get; }
        private async void ExecuteSyncRegenerationOrderRequestCommand()
        {
            try
            {
                IsBusy = true;

                var lastInventoryRepositoryModified = await _inventoryLocalRepository.GetLastModifiedDate();
                var lastCropTraitObservationRepositoryModified = await _cropTraitObservationLocalRepository.GetLastModifiedDate();

                var datetimeList = new List<DateTime> { lastInventoryRepositoryModified, lastCropTraitObservationRepositoryModified };
                var lastModified = datetimeList.Max();

                if (SelectedOrderRequest.last_synced_date == null || lastModified > SelectedOrderRequest.last_synced_date)
                {
                    var dbInventories = await _inventoryLocalRepository.GetPendingInventoriesByOrderRequestIdAsync(
                        SelectedOrderRequest.order_request_id,
                        SelectedOrderRequest.last_synced_date.GetValueOrDefault());

                    foreach (var dbInventory in dbInventories)
                    {
                        var inventory = new Inventory()
                        {
                            accession_id = dbInventory.AccessionId,
                            //accession_number = dbInventory.AccessionNumber,
                            //acc_name_col = string.Empty,
                            //acc_name_cul = string.Empty,
                            availability_status_code = dbInventory.AvailabilityStatusCode,
                            container_code = dbInventory.ContainerCode,
                            created_by = dbInventory.CreatedBy,
                            created_date = dbInventory.CreatedDate,
                            form_type_code = dbInventory.FormTypeCode,
                            inventory_id = dbInventory.InventoryId,
                            inventory_maint_policy_id = dbInventory.InventoryMaintPolicyId,
                            //inventory_number = dbInventory.InventoryNumber,
                            inventory_number_part1 = dbInventory.InventoryNumberPart1,
                            inventory_number_part2 = dbInventory.InventoryNumberPart2,
                            inventory_number_part3 = dbInventory.InventoryNumberPart3,
                            is_auto_deducted = dbInventory.IsAutoDeducted,
                            is_available = dbInventory.IsAvailable,
                            is_distributable = dbInventory.IsDistributable,
                            modified_by = dbInventory.ModifiedBy,
                            modified_date = dbInventory.ModifiedDate,
                            note = dbInventory.Note,
                            quantity_on_hand = dbInventory.QuantityOnHand,
                            quantity_on_hand_unit_code = dbInventory.QuantityOnHandUnitCode,
                            storage_location_part1 = dbInventory.StorageLocationPart1,
                            storage_location_part2 = dbInventory.StorageLocationPart2,
                            storage_location_part3 = dbInventory.StorageLocationPart3,
                            storage_location_part4 = dbInventory.StorageLocationPart4,
                            //taxonomy_species_code = string.Empty,
                            workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                            //order_request_id = dbInventory.OrderRequestId,
                        };

                        if(dbInventory.InventoryId > 0) //Update
                        {
                            //TODO: Download first and check possible conflicts
                            var result = await _restService.UpdateInventoryAsync(inventory);
                        }
                        else //Insert
                        {
                            var newInventoryIdStr = await _restService.CreateInventoryAsync(inventory);

                            var rows1 = await _inventoryLocalRepository.PatchInventoryIdAsync(dbInventory.Guid, int.Parse(newInventoryIdStr));
                            var rows2 = await _cropTraitObservationLocalRepository.PatchInventoryIdAsync(dbInventory.Guid, int.Parse(newInventoryIdStr));
                        }
                    }

                    var dbCropTraitObservations = await _cropTraitObservationLocalRepository.GetPendingCropTraitObservationByOrderRequestIdAsync(
                        SelectedOrderRequest.order_request_id,
                        SelectedOrderRequest.last_synced_date.GetValueOrDefault());

                    foreach (var dbCropTraitObservation in dbCropTraitObservations)
                    {
                        var cropTraitObservation = new CropTraitObservationInsertDto
                        {
                            crop_trait_code_id = dbCropTraitObservation.CropTraitCodeId,
                            crop_trait_id = dbCropTraitObservation.CropTraitId,
                            crop_trait_observation_id = dbCropTraitObservation.CropTraitObservationId,
                            inventory_id = dbCropTraitObservation.InventoryId,
                            numeric_value = dbCropTraitObservation.NumericValue,
                            string_value = dbCropTraitObservation.StringValue,
                            created_by = dbCropTraitObservation.CreatedBy,
                            created_date = dbCropTraitObservation.CreatedDate,
                            is_archived = "N",
                            method_id = 1,
                            workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                        };
                        if(cropTraitObservation.crop_trait_observation_id > 0)
                        {
                            var result = await _restService.UpdateCropTraitObservationAsync(cropTraitObservation);
                        }
                        else
                        {
                            var newCropTraitObservationIdStr = await _restService.CreateCropTraitObservationAsync(cropTraitObservation);
                            var rows1 = await _cropTraitObservationLocalRepository.PatchCropTraitObservationIdAsync(dbCropTraitObservation.Guid, int.Parse(newCropTraitObservationIdStr));
                        }
                    }

                    await _orderRequestRepository.PatchLastSyncedDateAsync(SelectedOrderRequest.order_request_id, lastModified);
                    SelectedOrderRequest.last_synced_date = lastModified;

                    //Newtonsoft.Json.JsonConvert.SerializeObject(dbInventories, Newtonsoft.Json.Formatting.Indented),
                    await PageDialogService.DisplayAlertAsync("Resultado de sincronización",
                        $"Se subieron los registros correctamente",
                        "OK");
                }
                else
                {
                    await PageDialogService.DisplayAlertAsync("Mensaje",
                        $"Todos los cambios están sincronizados",
                        "OK");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private async Task LoadOrderRequests()
        {
            var orderRequestBds = await _orderRequestRepository.GetOrderRequestsAsync();
            OrderRequestList = orderRequestBds.Select(x => new OrderRequest
            {
                order_request_id = x.OrderRequestId,
                local_number = x.LocalNumber,
                order_type_code = x.OrderTypeCode,
                last_synced_date = x.LastSyncedDate,
            }).ToList();
        }
        public DelegateCommand ExploreOrderRequestCommand { get; }
        private async void ExecuteExploreOrderRequestCommand()
        {
            try
            {
                IsBusy = true;

                var navResult = await NavigationService.NavigateAsync("DatabaseExplorerPage");
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SetLocalPrinterCommand { get; }
        private async void ExecuteSetLocalPrinterCommand()
        {
            try
            {
                IsBusy = true;

                var navigationResult = await NavigationService.NavigateAsync("BluetoothPrinterPickerPage");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                await LoadOrderRequests();

                if(Settings.SelectedRegenerationOrderRequestId > 0)
                {
                    SelectedOrderRequest = OrderRequestList.FirstOrDefault(x => x.order_request_id == Settings.SelectedRegenerationOrderRequestId);
                }
                if (!string.IsNullOrEmpty(Settings.SelectedRegenerationLocation))
                {
                    SelectedLocation = Settings.SelectedRegenerationLocation;
                }

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (!IsInitialized)
                return;
            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("SelectedPrinter"))
                {
                    BluetoothPrinterMac = parameters.GetValue<string>("SelectedPrinter");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
