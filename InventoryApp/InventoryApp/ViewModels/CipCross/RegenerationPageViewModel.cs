﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class RegenerationPageViewModel : ViewModelBaseZ
    {
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly IRestService _restService;

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        private Inventory _currentInventory;
        public Inventory CurrentInventory
        {
            get { return _currentInventory; }
            set { SetProperty(ref _currentInventory, value); }
        }
        private bool _hasResult;
        public bool HasResult
        {
            get { return _hasResult; }
            set { SetProperty(ref _hasResult, value); }
        }
        private ObservableCollection<InventoryAction> _inventoryActionList;
        public ObservableCollection<InventoryAction> InventoryActionList
        {
            get { return _inventoryActionList; }
            set { SetProperty(ref _inventoryActionList, value); }
        }
        public RegenerationPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService, IInventoryLocalRepository inventoryLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;
            _inventoryLocalRepository = inventoryLocalRepository;

            SearchCommand = new DelegateCommand(ExecuteSearchCommand).ObservesCanExecute(() => IsNotBusy);
            TransplantCommmand = new DelegateCommand(ExecuteTransplantCommmand).ObservesCanExecute(() => IsNotBusy);
            EvaluateCommmand = new DelegateCommand(ExecuteEvaluateCommmand).ObservesCanExecute(() => IsNotBusy);

            HasResult = false;
        }
        public DelegateCommand TransplantCommmand { get; }
        private async void ExecuteTransplantCommmand()
        {
            try
            {
                IsBusy = true;

                if (CurrentInventory == null)
                    return;

                var selectedItems = new List<Inventory>();
                selectedItems.Add(CurrentInventory);
                var navigationResult = await NavigationService.NavigateAsync("TransplantInventoriesPage", new NavigationParameters
                {
                    {"SelectedInventories", selectedItems }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;

            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public DelegateCommand EvaluateCommmand { get; }
        private async void ExecuteEvaluateCommmand()
        {
            try
            {
                IsBusy = true;

                if (CurrentInventory == null)
                    return;

                var selectedItems = new List<Inventory>();
                selectedItems.Add(CurrentInventory);
                var navigationResult = await NavigationService.NavigateAsync("EditRegenerationInventoryPage", new NavigationParameters
                {
                    {"SelectedInventories", selectedItems }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;

            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        
        public DelegateCommand SearchCommand { get; }
        private async void ExecuteSearchCommand()
        {
            try
            {
                IsBusy = true;

                if (string.IsNullOrWhiteSpace(SearchText))
                    return;

                CurrentInventory = null;
                
                var dbInventory = await _inventoryLocalRepository.GetInventoryByInventoryNumberAsync(SearchText);

                if(dbInventory == null)
                {
                    HasResult = false;
                }
                else
                {
                    CurrentInventory = new Inventory
                    {
                        accession_id = dbInventory.AccessionId,
                        accession_number = dbInventory.AccessionNumber,
                        acc_name_col = string.Empty,
                        acc_name_cul = string.Empty,
                        availability_status_code = dbInventory.AvailabilityStatusCode,
                        container_code = dbInventory.ContainerCode,
                        created_by = dbInventory.CreatedBy,
                        created_date = dbInventory.CreatedDate,
                        form_type_code = dbInventory.FormTypeCode,
                        inventory_id = dbInventory.InventoryId,
                        inventory_maint_policy_id = dbInventory.InventoryMaintPolicyId,
                        inventory_number = dbInventory.InventoryNumber,
                        inventory_number_part1 = dbInventory.InventoryNumberPart1,
                        inventory_number_part2 = dbInventory.InventoryNumberPart2,
                        inventory_number_part3 = dbInventory.InventoryNumberPart3,
                        is_auto_deducted = dbInventory.IsAutoDeducted,
                        is_available = dbInventory.IsAvailable,
                        is_distributable = dbInventory.IsDistributable,
                        modified_by = dbInventory.ModifiedBy,
                        modified_date = dbInventory.ModifiedDate,
                        note = dbInventory.Note,
                        quantity_on_hand = dbInventory.QuantityOnHand,
                        quantity_on_hand_unit_code = dbInventory.QuantityOnHandUnitCode,
                        storage_location_part1 = dbInventory.StorageLocationPart1,
                        storage_location_part2 = dbInventory.StorageLocationPart2,
                        storage_location_part3 = dbInventory.StorageLocationPart3,
                        storage_location_part4 = dbInventory.StorageLocationPart4,
                        taxonomy_species_code = string.Empty,
                        workgroup_cooperator_id = dbInventory.OwnedBy,
                        order_request_id = dbInventory.OrderRequestId,
                        Guid = dbInventory.Guid,
                    };
                    HasResult = true;

                    var tempInventoryActions = await _restService.GetInventoryActionsByInventoryIdAsync(CurrentInventory.inventory_id);
                    InventoryActionList = new ObservableCollection<InventoryAction>(tempInventoryActions);
                }
            }
            catch (Exception ex)
            {
                HasResult = false;
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
