﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace InventoryApp.ViewModels
{
    public class CrossingPageViewModel : ViewModelBaseZ
    {
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly ICrossingLocalRepository _crossingLocalRepository;
        private IEnumerable<InventoryDb> _allOrderRequestInventories;
        private CrossingDb CurrentCrossing;
        private IEnumerable<InventoryDb> _cacheInventotyDbList;

        private Syncfusion.SfDataGrid.XForms.SelectionMode selectionMode;
        public Syncfusion.SfDataGrid.XForms.SelectionMode SelectionMode
        {
            get { return selectionMode; }
            set { SetProperty(ref selectionMode, value); }
        }
        private InventoryDb _currentInventoryDb;
        public InventoryDb CurrentInventoryDb
        {
            get { return _currentInventoryDb; }
            set { SetProperty(ref _currentInventoryDb, value); }
        }
        #region Properties
        private IEnumerable<OrderRequestDb> _orderRequestDbList;
        public IEnumerable<OrderRequestDb> OrderRequestDbList
        {
            get { return _orderRequestDbList; }
            set { SetProperty(ref _orderRequestDbList, value); }
        }
        private OrderRequestDb _selectedOrderRequestDb;
        public OrderRequestDb SelectedOrderRequestDb
        {
            get { return _selectedOrderRequestDb; }
            set { SetProperty(ref _selectedOrderRequestDb, value); }
        }

        private IEnumerable<InventoryDb> _InventoryDbList;
        public IEnumerable<InventoryDb> InventoryDbList
        {
            get { return _InventoryDbList; }
            set { SetProperty(ref _InventoryDbList, value); }
        }
        private InventoryDb _selectedInventoryDb;
        public InventoryDb SelectedInventoryDb
        {
            get { return _selectedInventoryDb; }
            set { SetProperty(ref _selectedInventoryDb, value); }
        }
        private int _inventoryDbListCount;
        public int InventoryDbListCount
        {
            get { return _inventoryDbListCount; }
            set { SetProperty(ref _inventoryDbListCount, value); }
        }
        private IEnumerable<int> _femaleOrderList;
        public IEnumerable<int> FemaleOrderList
        {
            get { return _femaleOrderList; }
            set { SetProperty(ref _femaleOrderList, value); }
        }
        private int _selectedFemaleOrder;
        public int SelectedFemaleOrder
        {
            get { return _selectedFemaleOrder; }
            set
            {
                SetProperty(ref _selectedFemaleOrder, value);
                RaisePropertyChanged(nameof(Order));
            }
        }
        private IEnumerable<int> _maleOrderList;
        public IEnumerable<int> MaleOrderList
        {
            get { return _maleOrderList; }
            set { SetProperty(ref _maleOrderList, value); }
        }
        private int _selectedMaleOrder;
        public int SelectedMaleOrder
        {
            get { return _selectedMaleOrder; }
            set
            {
                SetProperty(ref _selectedMaleOrder, value);
                RaisePropertyChanged(nameof(Order));
            }
        }
        private IEnumerable<CodeValue> _crossingTypeCodeList;
        public IEnumerable<CodeValue> CrossingTypeCodeList
        {
            get { return _crossingTypeCodeList; }
            set { SetProperty(ref _crossingTypeCodeList, value); }
        }
        private CodeValue _selectedCrossingType;
        public CodeValue SelectedCrossingType
        {
            get { return _selectedCrossingType; }
            set
            {
                SetProperty(ref _selectedCrossingType, value);
                if(value != null && value.Code.Equals("SC"))
                {
                    IsSibCrossSelected = true;
                }
                else
                {
                    IsSibCrossSelected = false;
                }
                RaisePropertyChanged(nameof(Order));
            }
        }
        private bool _isSibCrossSelected;
        public bool IsSibCrossSelected
        {
            get { return _isSibCrossSelected; }
            set { SetProperty(ref _isSibCrossSelected, value); }
        }
        private string _crossingName;
        public string CrossingName
        {
            get { return _crossingName; }
            set { SetProperty(ref _crossingName, value); }
        }
        public string Order
        {
            get
            {
                string maleOrder = "?";
                if (SelectedCrossingType != null && !_isSibCrossSelected)
                {
                    maleOrder = SelectedCrossingType.Code;
                }
                else if (SelectedCrossingType != null && _isSibCrossSelected && _selectedMaleOrder > 0)
                {
                    maleOrder = _selectedMaleOrder.ToString();
                }
                return $"{(_selectedFemaleOrder > 0 ? _selectedFemaleOrder.ToString() : "?")} - {maleOrder}"; 
            }
        }
        private IEnumerable<int> _flowersCountList;
        public IEnumerable<int> FlowersCountList
        {
            get { return _flowersCountList; }
            set { SetProperty(ref _flowersCountList, value); }
        }
        private int _selectedFlowersCount;
        public int SelectedFlowersCount
        {
            get { return _selectedFlowersCount; }
            set { SetProperty(ref _selectedFlowersCount, value); }
        }
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        private DateTime _crossingDate;
        public DateTime CrossingDate
        {
            get { return _crossingDate; }
            set { SetProperty(ref _crossingDate, value); }
        }
        #endregion

        public CrossingPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository, ICrossingLocalRepository crossingLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _crossingLocalRepository = crossingLocalRepository;

            SelectionMode = Syncfusion.SfDataGrid.XForms.SelectionMode.Single;

            InventorySelectedCommand = new DelegateCommand(ExecuteInventorySelectedCommand).ObservesCanExecute(() => IsNotBusy);
            SearchCommand = new DelegateCommand(ExecuteSearchCommand).ObservesCanExecute(() => IsNotBusy);
            SaveAndPrintCommand = new DelegateCommand(ExecuteSaveAndPrintCommand).ObservesCanExecute(() => IsNotBusy);

            CrossingTypeCodeList = CodeValueFactory.CrossingTypeList;
            
            FlowersCountList = Enumerable.Range(1, 50).ToArray();
            CrossingDate = DateTime.Now;
        }
        
        public DelegateCommand InventorySelectedCommand { get; }
        private async void ExecuteInventorySelectedCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedInventoryDb == null)
                    return;

                InventorySelected();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private void InventorySelected()
        {
            var tempFemaleOrderList = Enumerable.Range(1, (int)SelectedInventoryDb.QuantityOnHand);

            //var tempFemaleOrderList = _allOrderRequestInventories
            //        .Where(x => x.InventoryNumberPart2 == SelectedInventoryDb.InventoryNumberPart2 &&
            //            !string.IsNullOrEmpty(x.InventoryNumberPart3) && x.InventoryNumberPart3.Contains('.'))
            //        .Select(x => x.InventoryNumberPart3.Split('.').Last());
            //FemaleOrderList = tempFemaleOrderList.Select(x => int.Parse(x)).ToList();
            FemaleOrderList = tempFemaleOrderList.ToArray();
            MaleOrderList = tempFemaleOrderList.ToArray();

            CrossingName = $"{SelectedInventoryDb.AccessionNumber} x {SelectedInventoryDb.AccessionNumber}";
        }
        public DelegateCommand SearchCommand { get; }
        private async void ExecuteSearchCommand()
        {
            try
            {
                IsBusy = true;

                if (string.IsNullOrEmpty(SearchText))
                    return;

                var rgxSourceInventoryId = new Regex(@"^\d*$");
                var rgxInventoryNumber = new Regex(@"^\d* \d*$");
                var rgxInventoryNumberWithOrder = new Regex(@"^\d* \d*.\d*$");
                var withOrder = false;

                if (SearchText.Length == 36)
                {
                    SelectedInventoryDb = null;
                    CurrentCrossing = await _crossingLocalRepository.GetInventoryByGuidAsync(SearchText);
                    if(CurrentCrossing == null)
                    {
                        await PageDialogService.DisplayAlertAsync("Resultado de búsqueda", "No se ha encontrado ninguna coincidencia", "OK");
                    }
                    else
                    {
                        CrossingName = CurrentCrossing.CrossingName;
                        SelectedCrossingType = CrossingTypeCodeList.FirstOrDefault(x => x.Code.Equals(CurrentCrossing.CrossingTypeCode));
                        SelectedFemaleOrder = CurrentCrossing.FemaleOrder;
                        if (IsSibCrossSelected)
                            SelectedMaleOrder = CurrentCrossing.MaleOrder;
                        SelectedFlowersCount = CurrentCrossing.PollinatedFlowers.GetValueOrDefault(0);
                        CrossingDate = CurrentCrossing.CrossingDate.GetValueOrDefault();
                    }
                    return;
                }
                else if (rgxSourceInventoryId.IsMatch(SearchText))
                {
                    SelectedInventoryDb = InventoryDbList.FirstOrDefault(x => x.InventoryNumberPart2.ToString().Equals(SearchText));
                }
                else if (rgxInventoryNumber.IsMatch(SearchText))
                {
                    SelectedInventoryDb = InventoryDbList.FirstOrDefault(x => $"{x.InventoryNumberPart2} {x.InventoryNumberPart3}".Equals(SearchText));
                }
                else if (rgxInventoryNumberWithOrder.IsMatch(SearchText))
                {
                    var formattedSearchText = SearchText.Substring(0, SearchText.LastIndexOf('.'));
                    SelectedInventoryDb = InventoryDbList.FirstOrDefault(x => $"{x.InventoryNumberPart2} {x.InventoryNumberPart3}".Equals(formattedSearchText));
                    withOrder = true;
                }
                else
                {
                    throw new Exception("Formato de código de barras inválido");
                }

                if (SelectedInventoryDb != null)
                {
                    InventorySelected();
                    if (withOrder)
                    {
                        var order = SearchText.Split('.').Last();
                        SelectedFemaleOrder = FemaleOrderList.FirstOrDefault(x => x.ToString().Equals(order));
                    }
                }
                else
                    await PageDialogService.DisplayAlertAsync("Resultado de búsqueda", "No se ha encontrado ninguna coincidencia", "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SaveAndPrintCommand { get; }
        private async void ExecuteSaveAndPrintCommand()
        {
            try
            {
                IsBusy = true;

                if(CurrentCrossing == null)
                {
                    if (SelectedInventoryDb == null)
                        throw new Exception("Ningún inventario madre ha sido seleccinado");

                    var newCrossing = new CrossingDb
                    {
                        CooperatorId = Settings.UserCooperatorId,
                        CooperatorName = Settings.Username,
                        CrossingDate = CrossingDate,
                        CrossingName = CrossingName,
                        CrossingType = SelectedCrossingType.Value,
                        CrossingTypeCode = SelectedCrossingType.Code,
                        FemaleAccessionNumber = SelectedInventoryDb.AccessionNumber,
                        FemaleInventoryId = SelectedInventoryDb.InventoryId,
                        FemaleOrder = SelectedFemaleOrder,
                        Guid = Guid.NewGuid().ToString(),
                        Locality = "",
                        MaleAccessionNumber = SelectedInventoryDb.AccessionNumber,
                        MaleOrder = SelectedMaleOrder,
                        ParentInventoryId = SelectedInventoryDb.ParentInventoryId,
                        PollinatedFlowers = SelectedFlowersCount,
                        PrintedDate = DateTime.Now,
                        SourceInventoryId = SelectedInventoryDb.InventoryNumberPart2,
                        OrderRequestId = SelectedOrderRequestDb.OrderRequestId,
                        FemaleParentGuid = SelectedInventoryDb.Guid,
                        MaleParentGuid = SelectedInventoryDb.Guid,
                    };
                    var affectedRows = await _crossingLocalRepository.InsertInventoryAsync(newCrossing);

                    if (affectedRows > 0)
                    {
#if DEBUG
                        await Xamarin.Essentials.Clipboard.SetTextAsync(newCrossing.Guid);
#endif
                        await PageDialogService.DisplayAlertAsync("Resultado de guardar e imprimir", $"Se registró el cruzamiento satisfactoriamente\nGuid : {newCrossing.Guid}", "OK");

                        //SelectedCrossingType = null;
                        SelectedFemaleOrder = 0;
                        SelectedMaleOrder = 0;
                        CrossingName = string.Empty;
                        SelectedFlowersCount = 0;
                        CrossingDate = DateTime.Now;

                        //Clear selection
                        SelectedInventoryDb = null;
                        InventoryDbList = _cacheInventotyDbList.ToList(); //TODO: send event to refresh datagrid

                        //SelectionMode = Syncfusion.SfDataGrid.XForms.SelectionMode.None;
                        //SelectionMode = Syncfusion.SfDataGrid.XForms.SelectionMode.Single;
                    }
                    else
                        throw new Exception("Error no esperado");
                }
                else
                {
                    CurrentCrossing.PollinatedFlowers = SelectedFlowersCount;
                    CurrentCrossing.CrossingDate = CrossingDate;
                    CurrentCrossing.PrintedDate = DateTime.Now;

                    var updatedRows = await _crossingLocalRepository.UpdateInventoryAsync(CurrentCrossing);
                    if(updatedRows > 0)
                    {
                        await PageDialogService.DisplayAlertAsync("Resultado de guardar e imprimir", $"Se actualizó el cruzamiento satisfactoriamente\nGuid : {CurrentCrossing.Guid}", "OK");
                        CurrentCrossing = null;

                        SelectedCrossingType = null;
                        SelectedFemaleOrder = 0;
                        SelectedMaleOrder = 0;
                        CrossingName = string.Empty;
                        SelectedFlowersCount = 0;
                        CrossingDate = DateTime.Now;
                    }
                }

               
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (Settings.SelectedRegenerationOrderRequestId == -1)
                    throw new Exception("Seleccionar solicitud en Ajustes de regeneración");

                SelectedOrderRequestDb = await _orderRequestRepository.GetOrderRequestById(Settings.SelectedRegenerationOrderRequestId);
                if (SelectedOrderRequestDb == null)
                    throw new Exception("Solicitud no encontrada\nIr a Ajustes de regeneración");

                _cacheInventotyDbList = await _inventoryLocalRepository.GetMotherInventoriesByOrderRequestIdAsync(SelectedOrderRequestDb.OrderRequestId);
                InventoryDbList = _cacheInventotyDbList.ToList();
                InventoryDbListCount = InventoryDbList.Count();

                SelectedCrossingType = CrossingTypeCodeList.FirstOrDefault(x => x.Code.Equals("SC"));

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
