﻿using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class CrossingDatabase : ICrossingLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<CrossingDb>();
        }
        public async Task<int> InsertInventoryAsync(CrossingDb InventoryDb)
        {
            await Init();
            return await Database.InsertAsync(InventoryDb);
        }

        public async Task<int> UpdateInventoryAsync(CrossingDb InventoryDb)
        {
            await Init();
            return await Database.UpdateAsync(InventoryDb);
        }

        public async Task<CrossingDb> GetInventoryByGuidAsync(string guid)
        {
            await Init();
            var result = await Database.Table<CrossingDb>()
                .FirstOrDefaultAsync(x => x.Guid.Equals(guid));
            return result;
        }

        public async Task<IEnumerable<CrossingDb>> GetCrossingByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            var result = await Database.Table<CrossingDb>()
                .Where(x => x.OrderRequestId == orderRequestId)
                .ToListAsync();
            return result;
        }

        public async Task<int> DeleteAllAsync()
        {
            await Init();
            return await Database.DeleteAllAsync<CrossingDb>();
        }

        public async Task<IEnumerable<CrossingDb>> GetAllAsync()
        {
            await Init();
            return await Database.Table<CrossingDb>().ToArrayAsync();
        }
    }
}
