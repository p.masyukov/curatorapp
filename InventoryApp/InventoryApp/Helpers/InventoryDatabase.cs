﻿using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class InventoryDatabase : IInventoryLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<InventoryDb>();
        }
        public async Task<List<InventoryDb>> GetInventoriesAsync()
        {
            await Init();
            var result = await Database.Table<InventoryDb>().ToListAsync();
            return result;
        }
        public async Task<List<InventoryDb>> GetInventoriesByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .Where(x => x.OrderRequestId == orderRequestId)
                .ToListAsync();
            return result;
        }
        public async Task<IEnumerable<InventoryDb>> GetMotherInventoriesByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .Where(x => x.OrderRequestId == orderRequestId && x.IsMother && !x.InventoryNumberPart3.Contains("."))
                .ToListAsync();
            return result;
        }
        public async Task<InventoryDb> GetInventoryByInventoryNumberAsync(string inventoryNumber)
        {
            await Init();
            var result = (await Database
                .QueryAsync<InventoryDb>($"SELECT * FROM inventory WHERE InventoryNumberPart1 || ' ' || InventoryNumberPart2 || ' ' || InventoryNumberPart3 = '{inventoryNumber}'")
                ).FirstOrDefault();
            //.Table<InventoryDb>()
            //.Where(x => (x.InventoryNumberPart1 + x.InventoryNumberPart2 + x.InventoryNumberPart3).Equals(inventoryNumber))

            return result;
        }
        public async Task<InventoryDb> GetInventoryByGuidAsync(string guid)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .FirstOrDefaultAsync(x => x.Guid.Equals(guid));
            return result;
        }
        public async Task<int> InsertInventoryAsync(InventoryDb InventoryDb)
        {
            await Init();
            return await Database.InsertAsync(InventoryDb);
        }
        public async Task<int> InsertInventoryManyAsync(IEnumerable<InventoryDb> InventoryDbs)
        {
            await Init();
            return await Database.InsertAllAsync(InventoryDbs);
        }
        public async Task<int> DeleteInventoryByIdAsync(int inventoryId)
        {
            await Init();
            return await Database.ExecuteAsync($"DELETE FROM inventory WHERE InventoryId = {inventoryId}");
        }
        public async Task<int> DeleteInventoryByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            return await Database.ExecuteAsync($"DELETE FROM inventory WHERE OrderRequestId = {orderRequestId}");
        }
        public async Task<int> GetMinorInventoryIdAsync()
        {
            await Init();
            return await Database.ExecuteScalarAsync<int>($"SELECT MIN(InventoryId) FROM inventory");
        }
        public async Task<DateTime> GetLastModifiedDate()
        {
            await Init();
            return await Database.ExecuteScalarAsync<DateTime>($"SELECT MAX(coalesce(ModifiedDate, CreatedDate)) FROM inventory");
        }
        public async Task<List<InventoryDb>> GetPendingInventoriesByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .Where(x => x.OrderRequestId == orderRequestId &&
                (x.CreatedDate > lastSynced || x.ModifiedDate > lastSynced))
                .ToListAsync();
            return result;
        }
        public async Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId)
        {
            await Init();
            return await Database.ExecuteAsync($"UPDATE inventory SET InventoryId = {newInventoryId} WHERE Guid = '{inventoryGuid}'");
        }

        public async Task<int> UpdateInventoryAsync(InventoryDb InventoryDb)
        {
            await Init();
            return await Database.UpdateAsync(InventoryDb);
        }

        public async Task<int> PatchQuantityAsync(string guid, decimal quantity, int modifiedBy, DateTime modifiedDate)
        {
            await Init();
            var inventory = await Database.Table<InventoryDb>().FirstOrDefaultAsync(x => x.Guid.Equals(guid));
            inventory.QuantityOnHand = quantity;
            inventory.ModifiedBy = modifiedBy;
            inventory.ModifiedDate = modifiedDate;

            return await Database.UpdateAsync(inventory);
        }
    }
}
