﻿using InventoryApp.Interfaces;
using Syncfusion.SfDataGrid.XForms;
using Syncfusion.SfDataGrid.XForms.Exporting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace InventoryApp.Helpers
{
    public static class UserInterfaceUtils
    {
        public static void ExportToExcel(SfDataGrid dataGrid)
        {
            DataGridExcelExportingController excelExport = new DataGridExcelExportingController();
            DataGridExcelExportingOption options = new DataGridExcelExportingOption();
            var list = new List<string> { "IsSelected", "" };
            options.ExcludedColumns = list;

            var excelEngine = excelExport.ExportToExcel(dataGrid, options);
            if (excelEngine == null)
            {
                return;
            }
            var workbook = excelEngine.Excel.Workbooks[0];
            if (workbook == null)
            {
                return;
            }
            MemoryStream stream = new MemoryStream();
            workbook.SaveAs(stream);
            workbook.Close();
            excelEngine.Dispose();

            string tempFileName = $@"{DateTime.Now:yyyyMMdd_HHmmss}_{DateTime.Now.Ticks.GetHashCode().ToString("x").ToUpper()}.xlsx";
            if (Device.RuntimePlatform == Device.UWP)
            {
                Xamarin.Forms.DependencyService.Get<ISaveWindowsPhone>().Save(tempFileName, "application/msexcel", stream);
            }
            else
            {
                Xamarin.Forms.DependencyService.Get<ISave>().Save(tempFileName, "application/msexcel", stream);
            }
        }
    }
}
