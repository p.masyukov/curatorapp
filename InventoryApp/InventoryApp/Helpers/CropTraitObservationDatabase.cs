﻿using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class CropTraitObservationDatabase : ICropTraitObservationLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<CropTraitObservationDb>();
        }
        public async Task<IEnumerable<CropTraitObservationDb>> GetAllCropTraitObservationAsync()
        {
            await Init();
            var result = await Database.Table<CropTraitObservationDb>()
                .ToArrayAsync();
            return result;
        }
        public async Task<int> DeteleAllCropTraitObservationAsync()
        {
            await Init();
            var result = await Database.DeleteAllAsync<CropTraitObservationDb>();
            return result;
        }
        public async Task<CropTraitObservationDb> GetCropTraitObservationByGuidAsync(Guid guid)
        {
            await Init();
            var result = await Database.Table<CropTraitObservationDb>()
                .FirstOrDefaultAsync(x => x.Guid == guid);
            return result;
        }

        public async Task<IEnumerable<CropTraitObservationDb>> GetCropTraitObservationByInventoryGuidAsync(string inventoryGuid)
        {
            await Init();
            var result = await Database.Table<CropTraitObservationDb>()
                .Where(x => x.InventoryGuid.Equals(inventoryGuid))
                .ToListAsync();
            return result;
        }

        public async Task<IEnumerable<CropTraitObservationDb>> GetCropTraitObservationByManyInventoryGuidAsync(string[] inventoryGuids)
        {
            await Init();
            var result = await Database.Table<CropTraitObservationDb>()
               .Where(x => inventoryGuids.Contains(x.InventoryGuid))
               .ToListAsync();
            return result;
        }

        public async Task<int> InsertCropTraitObservationAsync(CropTraitObservationDb cropTraitObservationDb)
        {
            await Init();
            return await Database.InsertAsync(cropTraitObservationDb);
        }
        public async Task<int> InsertCropTraitObservationManyAsync(IEnumerable<CropTraitObservationDb> cropTraitObservationDbs)
        {
            await Init();
            return await Database.InsertAllAsync(cropTraitObservationDbs);
        }
        public async Task<int> UpdateCropTraitObservationAsync(CropTraitObservationDb cropTraitObservationDb)
        {
            await Init();
            return await Database.UpdateAsync(cropTraitObservationDb);
        }

        public async Task<DateTime> GetLastModifiedDate()
        {
            await Init();
            return await Database.ExecuteScalarAsync<DateTime>($"SELECT MAX(coalesce(ModifiedDate, CreatedDate)) FROM crop_trait_observation");
        }
        public async Task<List<CropTraitObservationDb>> GetPendingCropTraitObservationByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced)
        {
            await Init();

            var sql = $@"
SELECT * from crop_trait_observation cto
WHERE cto.InventoryGuid in (SELECT i.Guid FROM inventory i WHERE OrderRequestId = {orderRequestId})
";
            //AND coalesce(cto.ModifiedDate, cto.CreatedDate) > '{lastSynced.ToUniversalTime()}'
            //AND (cto.ModifiedDate > '{lastSynced}' OR cto.CreatedDate > '{lastSynced}')
            var result = await Database.QueryAsync<CropTraitObservationDb>(sql);

            //var result = await Database.Table<CropTraitObservationDb>()
            //    .Where(x => x.OrderRequestId == orderRequestId &&
            //    (x.CreatedDate > lastSynced || x.ModifiedDate > lastSynced))
            //    .ToListAsync();
            return result;
        }

        public async Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId)
        {
            await Init();

            var sql = $"UPDATE crop_trait_observation SET InventoryId = {newInventoryId} WHERE InventoryGuid = '{inventoryGuid}'";
            var result = await Database.ExecuteAsync(sql);

            return result;
        }

        public async Task<int> PatchCropTraitObservationIdAsync(Guid guid, int newCropTraitObservationId)
        {
            await Init();
            var sql = $"UPDATE crop_trait_observation SET CropTraitObservationId = {newCropTraitObservationId} WHERE Guid = '{guid}'";
            var result = await Database.ExecuteAsync(sql);
            return result;
        }

        public async Task<IEnumerable<CropTraitObservationDb>> GetCropTraitObservationByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();

            var sql = $@"
SELECT * from crop_trait_observation cto
WHERE cto.InventoryGuid in (SELECT i.Guid FROM inventory i WHERE OrderRequestId = {orderRequestId})
";
            var result = await Database.QueryAsync<CropTraitObservationDb>(sql);
            return result;
        }
    }
}
