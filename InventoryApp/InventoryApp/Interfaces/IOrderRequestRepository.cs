﻿using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface IOrderRequestRepository
    {
        Task<List<OrderRequestDb>> GetOrderRequestsAsync();
        Task<OrderRequestDb> GetOrderRequestById(int orderRequestId);
        Task<int> InsertItemAsync(OrderRequestDb orderRequestDb);
        Task<int> DeleteOrderRequestByIdAsync(int orderRequestId);
        Task<int> PatchLastSyncedDateAsync(int orderRequestId, DateTime lastSyncedDate);
    }
}
