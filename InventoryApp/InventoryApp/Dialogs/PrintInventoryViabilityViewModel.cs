﻿using InventoryApp.Helpers;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventoryApp.Dialogs
{
    public class PrintInventoryViabilityViewModel : BindableBase, IDialogAware
    {
        public event Action<IDialogParameters> RequestClose;
        private RestClient _restClient;

        #region Properties
        private Printer _printer;
        public Printer Printer
        {
            get { return _printer; }
            set { SetProperty(ref _printer, value); }
        }

        private int _rowsPerRecord;
        public int RowsPerRecord
        {
            get { return _rowsPerRecord; }
            set { SetProperty(ref _rowsPerRecord, value); }
        }

        private LabelTemplate _labelTemplate;
        public LabelTemplate LabelTemplate
        {
            get { return _labelTemplate; }
            set { SetProperty(ref _labelTemplate, value); }
        }

        private List<Printer> _printerList;
        public List<Printer> PrinterList
        {
            get { return _printerList; }
            set { SetProperty(ref _printerList, value); }
        }

        private List<LabelTemplate> _labelTemplateList;
        public List<LabelTemplate> LabelTemplateList
        {
            get { return _labelTemplateList; }
            set { SetProperty(ref _labelTemplateList, value); }
        }
        #endregion

        public PrintInventoryViabilityViewModel()
        {
            SubmitCommand = new DelegateCommand(OnSubmitTapped).ObservesCanExecute(() => CanContinue);
            CancelCommand = new DelegateCommand(OnCancelTapped);

            _restClient = new RestClient();
        }
        public DelegateCommand SubmitCommand { get; }
        public DelegateCommand CancelCommand { get; }
        private void OnSubmitTapped()
        {
            RequestClose(null);
        }
        public bool CanContinue
        {
            get { return true; }
        }
        private bool _cancelled;
        private void OnCancelTapped()
        {
            _cancelled = true;
            RequestClose(null);
        }
        public bool CanCloseDialog()
        {
            return _cancelled || CanContinue;
        }

        public void OnDialogClosed()
        {
            Console.WriteLine("The ChangePasswordDialog has been closed...");
        }

        public async void OnDialogOpened(IDialogParameters parameters)
        {
            try
            {
                if (PrinterList == null)
                {
                    PrinterList = await _restClient.GetPrinterList();
                }
                if (Printer == null)
                {
                    Printer = PrinterList.FirstOrDefault(p => Settings.Printer.Equals(p.PrinterName));
                    if (Printer == null)
                        Settings.Printer = string.Empty;
                }
                if (LabelTemplateList == null)
                {
                    LabelTemplateList = await _restClient.GetLabelTemplateList();
                }

                if (parameters.ContainsKey("ViabilityRequestItemList"))
                {
                    //_inventoryList = (List<InventoryThumbnail>)parameters["InventoryThumbnailList"];
                }
            }
            catch (Exception ex)
            {
                //await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
