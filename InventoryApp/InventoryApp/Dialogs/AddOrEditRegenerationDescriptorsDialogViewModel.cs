﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventoryApp.Dialogs
{
    public class AddOrEditRegenerationDescriptorsDialogViewModel : BindableBase, IDialogAware
    {
        ICropTraitObservationLocalRepository _cropTraitObservationLocalRepository;
        public CropTraitObservationDb CropTraitObservationRegStatus { get; set; }
        public CropTraitObservationDb CropTraitObservationRegNotes { get; set; }
        public InventoryDbWithTraits SelectedInventoryThumbnail { get; set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }
        private IEnumerable<Lookup> _regenerationStatusCodeList;
        public IEnumerable<Lookup> RegenerationStatusCodeList
        {
            get { return _regenerationStatusCodeList; }
            set { SetProperty(ref _regenerationStatusCodeList, value); }
        }
        private Lookup _selectedRegenarionStatusCode;
        public Lookup SelectedRegenarionStatusCode
        {
            get { return _selectedRegenarionStatusCode; }
            set { SetProperty(ref _selectedRegenarionStatusCode, value); }
        }
        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }

        public AddOrEditRegenerationDescriptorsDialogViewModel(ICropTraitObservationLocalRepository cropTraitObservationLocalRepository)
        {
            _cropTraitObservationLocalRepository = cropTraitObservationLocalRepository;

            SelectCommand = new DelegateCommand(ExecuteSelectCommand);
            CancelCommand = new DelegateCommand(ExecuteCancelCommand);

            RegenerationStatusCodeList = new List<Lookup>()
            {
                new Lookup { value_member = 2785, display_member = "No viable"},
                new Lookup { value_member = 2784, display_member = "Viable"}
            };
        }
        public DelegateCommand CancelCommand { get; }
        public DelegateCommand SelectCommand { get; }
        private void ExecuteCancelCommand()
        {
            RequestClose(null);
        }
        private async void ExecuteSelectCommand()
        {
            try
            {
                if(CropTraitObservationRegStatus == null)
                {
                    var newGuid = Guid.NewGuid();
                    var insertedRows = await _cropTraitObservationLocalRepository.InsertCropTraitObservationAsync(new CropTraitObservationDb
                    {
                        CropTraitCodeId = SelectedRegenarionStatusCode.value_member,
                        DisplayText = SelectedRegenarionStatusCode.DisplayMember,
                        Code = SelectedRegenarionStatusCode.DisplayMember,
                        CropTraitId = 2011,
                        CropTraitObservationId = -1,
                        Guid = newGuid,
                        InventoryGuid = SelectedInventoryThumbnail.InventoryDb.Guid,
                        InventoryId = SelectedInventoryThumbnail.InventoryDb.InventoryId,
                        NumericValue = null,
                        StringValue = null,
                        CreatedBy = Settings.UserCooperatorId,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedBy = Settings.UserCooperatorId,
                        ModifiedDate = DateTime.UtcNow,
                    });
                    SelectedInventoryThumbnail.CropTraitObservations[2011].Guid = newGuid;
                }
                else
                {
                    CropTraitObservationRegStatus.DisplayText = SelectedRegenarionStatusCode.DisplayMember;
                    CropTraitObservationRegStatus.CropTraitCodeId = SelectedRegenarionStatusCode.value_member;
                    CropTraitObservationRegStatus.ModifiedBy = Settings.UserCooperatorId;
                    CropTraitObservationRegStatus.ModifiedDate = DateTime.UtcNow;
                    await _cropTraitObservationLocalRepository.UpdateCropTraitObservationAsync(CropTraitObservationRegStatus);
                }
                SelectedInventoryThumbnail.CropTraitObservations[2011].DisplayText = SelectedRegenarionStatusCode.DisplayMember;
                SelectedInventoryThumbnail.CropTraitObservations[2011].crop_trait_code_id = SelectedRegenarionStatusCode.value_member;

                if(CropTraitObservationRegNotes == null)
                {
                    if (!string.IsNullOrEmpty(Notes))
                    {
                        var newGuid = Guid.NewGuid();
                        var insertedRows = await _cropTraitObservationLocalRepository.InsertCropTraitObservationAsync(new CropTraitObservationDb
                        {
                            DisplayText = Notes,
                            Code = null,
                            CropTraitId = 2013,
                            CropTraitCodeId = null,
                            CropTraitObservationId = -1,
                            Guid = newGuid,
                            InventoryGuid = SelectedInventoryThumbnail.InventoryDb.Guid,
                            InventoryId = SelectedInventoryThumbnail.InventoryDb.InventoryId,
                            NumericValue = null,
                            StringValue = Notes,
                            CreatedBy = Settings.UserCooperatorId,
                            CreatedDate = DateTime.UtcNow,
                            ModifiedBy = Settings.UserCooperatorId,
                            ModifiedDate = DateTime.UtcNow,
                        });
                        SelectedInventoryThumbnail.CropTraitObservations[2013].Guid = newGuid;
                    }
                }
                else
                {
                    CropTraitObservationRegNotes.DisplayText = string.IsNullOrEmpty(Notes) ? " " : Notes;
                    CropTraitObservationRegNotes.StringValue = string.IsNullOrEmpty(Notes) ? " " : Notes;
                    CropTraitObservationRegNotes.ModifiedBy = Settings.UserCooperatorId;
                    CropTraitObservationRegNotes.ModifiedDate = DateTime.UtcNow;
                    await _cropTraitObservationLocalRepository.UpdateCropTraitObservationAsync(CropTraitObservationRegNotes);
                }
                SelectedInventoryThumbnail.CropTraitObservations[2013].DisplayText = Notes;
                SelectedInventoryThumbnail.CropTraitObservations[2013].string_value = Notes;

                RequestClose(null);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
        }

        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {

        }

        public async void OnDialogOpened(IDialogParameters parameters)
        {
            try
            {
                Title = "";

                if (parameters.ContainsKey("SelectedInventory"))
                {
                    SelectedInventoryThumbnail = parameters.GetValue<InventoryDbWithTraits>("SelectedInventory");

                    var cropTraitObservations = await _cropTraitObservationLocalRepository.GetCropTraitObservationByInventoryGuidAsync(SelectedInventoryThumbnail.InventoryDb.Guid);
                    CropTraitObservationRegStatus = cropTraitObservations.FirstOrDefault(x => x.CropTraitId == 2011);
                    CropTraitObservationRegNotes = cropTraitObservations.FirstOrDefault(x => x.CropTraitId == 2013);

                    if(CropTraitObservationRegStatus != null)
                        SelectedRegenarionStatusCode = RegenerationStatusCodeList.FirstOrDefault(x => x.value_member == CropTraitObservationRegStatus.CropTraitCodeId);
                    else
                        SelectedRegenarionStatusCode = RegenerationStatusCodeList.FirstOrDefault(x => x.display_member.Equals("Viable"));

                    if (CropTraitObservationRegNotes != null)
                        Notes = CropTraitObservationRegNotes.StringValue;
                    else
                        Notes = string.Empty;
                }
                else
                    throw new Exception("SelectedInventory is required");

                //if (parameters.ContainsKey("CropTraitObservationRegStatus"))
                //{
                //    var CropTraitObservationRegStatus = parameters.GetValue<CropTraitObservationDb>("CropTraitObservationRegStatus");
                //    if (CropTraitObservationRegStatus != null)
                //        SelectedRegenarionStatusCode = RegenerationStatusCodeList.FirstOrDefault(x => x.value_member == CropTraitObservationRegStatus.CropTraitCodeId);
                //    else
                //        SelectedRegenarionStatusCode = RegenerationStatusCodeList.FirstOrDefault(x => x.display_member.Equals("Viable"));
                //}
                //else
                //{
                //    throw new Exception(" is required");
                //}

                //if (parameters.ContainsKey("CropTraitObservationRegNotes"))
                //{
                //    var CropTraitObservationRegNotes = parameters.GetValue<CropTraitObservationDb>("CropTraitObservationRegNotes");

                //    if (CropTraitObservationRegNotes != null)
                //        Notes = CropTraitObservationRegNotes.StringValue;
                //    else
                //        Notes = string.Empty;
                //}
                //else
                //{
                //    throw new Exception(" is required");
                //}
            }
            catch (Exception ex)
            {
                Message = ex.Message + Environment.NewLine + ex.InnerException?.Message;
            }
            finally
            {

            }
        }
    }
}
