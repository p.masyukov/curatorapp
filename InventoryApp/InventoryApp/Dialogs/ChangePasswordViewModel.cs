﻿using InventoryApp.Helpers;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Dialogs
{
    public class ChangePasswordViewModel : BindableBase, IDialogAware
    {
        public event Action<IDialogParameters> RequestClose;
        private RestClient _restClient;
        private string _server;

        #region Properties
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }
        private bool _isMessageVisible;
        public bool IsMessageVisible
        {
            get { return _isMessageVisible; }
            set { SetProperty(ref _isMessageVisible, value); }
        }
        
        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); RaisePropertyChanged(nameof(CanContinue)); }
        }
        private string _currentPassword;
        public string CurrentPassword
        {
            get { return _currentPassword; }
            set { SetProperty(ref _currentPassword, value); RaisePropertyChanged(nameof(CanContinue)); }
        }
        private string _newPassword;
        public string NewPassword
        {
            get { return _newPassword; }
            set { SetProperty(ref _newPassword, value); RaisePropertyChanged(nameof(CanContinue)); }
        }
        private string _confirmPassword;
        public string ConfirmPassword
        {
            get { return _confirmPassword; }
            set { SetProperty(ref _confirmPassword, value); RaisePropertyChanged(nameof(CanContinue)); }
        }
        #endregion

        #region LangProperties
        private string _uxUsernameLabel;
        public string UxUsernameLabel
        {
            get { return _uxUsernameLabel; }
            set { SetProperty(ref _uxUsernameLabel, value); }
        }
        private string _uxCurrentPasswordLabel;
        public string UxCurrentPasswordLabel
        {
            get { return _uxCurrentPasswordLabel; }
            set { SetProperty(ref _uxCurrentPasswordLabel, value); }
        }
        private string _uxNewPasswordLabel;
        public string UxNewPasswordLabel
        {
            get { return _uxNewPasswordLabel; }
            set { SetProperty(ref _uxNewPasswordLabel, value); }
        }
        private string _uxConfirmPassswordLabel;
        public string UxConfirmPasswordLabel
        {
            get { return _uxConfirmPassswordLabel; }
            set { SetProperty(ref _uxConfirmPassswordLabel, value); }
        }
        private string _uxCancelButton;
        public string UxCancelButton
        {
            get { return _uxCancelButton; }
            set { SetProperty(ref _uxCancelButton, value); }
        }
        private string _uxOkButton;
        public string UxOkButton
        {
            get { return _uxOkButton; }
            set { SetProperty(ref _uxOkButton, value); }
        }
        #endregion
        public ChangePasswordViewModel()
        {
            Message = "";
            SubmitCommand = new DelegateCommand(OnSubmitTapped).ObservesCanExecute(() => CanContinue);
            CancelCommand = new DelegateCommand(OnCancelTapped);

            if(Settings.LangId == 2)
            {
                Title = "Cambiar Contraseña";
                UxCancelButton = "Cancelar";
                UxOkButton = "Cambiar";
                UxUsernameLabel = "Usuario";
                UxCurrentPasswordLabel = "Contraseña actual";
                UxNewPasswordLabel = "Nueva contraseña";
                UxConfirmPasswordLabel = "Confirmar contraseña";
            }
            else
            {
                Title = "Change Password";
                UxCancelButton = "Cancel";
                UxOkButton = "Change";
                UxUsernameLabel = "Username";
                UxCurrentPasswordLabel = "Current password";
                UxNewPasswordLabel = "New password";
                UxConfirmPasswordLabel = "Confirm password";
            }
            IsMessageVisible = false;
            _restClient = new RestClient();
        }
        public DelegateCommand SubmitCommand { get; }
        public DelegateCommand CancelCommand { get; }
        public bool CanContinue
        {
            get { return !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(CurrentPassword) && !string.IsNullOrEmpty(NewPassword) && !string.IsNullOrEmpty(ConfirmPassword); }
        }

        private async void OnSubmitTapped()
        {
            try
            {
                IsMessageVisible = false;

                if (string.IsNullOrEmpty(Settings.Server))
                    throw new Exception("Server is empty");
                if (!NewPassword.Equals(ConfirmPassword))
                    throw new Exception("New and Confirm password do not match");
                
                var resp = await _restClient.ChangePassword(Username, CurrentPassword, NewPassword, Settings.Server);

                RequestClose(new DialogParameters { { "Status", true } });
                //RequestClose(null);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                IsMessageVisible = true;
            }
        }

        private bool _cancelled;
        private void OnCancelTapped()
        {
            _cancelled = true;
            RequestClose(null);
        }

        public bool CanCloseDialog()
        {
            return _cancelled || CanContinue;
        }

        public void OnDialogClosed()
        {
            Console.WriteLine("The ChangePasswordDialog has been closed...");
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            if (parameters.ContainsKey("Server"))
                _server = parameters.GetValue<string>("Server");
            if (parameters.ContainsKey("Username"))
                Username = parameters.GetValue<string>("Username");
        }
    }
}
